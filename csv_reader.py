#!/usr/bin/env python3
# coding: utf-8
import csv

class csv_reader:

    def csv2dict(self, file_name):
	result = list()
	
        with open(file_name) as csvfile:
		dict_reader = csv.DictReader(csvfile, skipinitialspace=True, delimiter=';')
		for row in dict_reader:
	    		result.append(row)
	
	return result
