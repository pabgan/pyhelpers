#!/usr/bin/env python3
# coding: utf-8
import re
from collections import OrderedDict

# Returns a dictionary with {"TABLE": table_name, "COLUMN1": val1, "COLUMN2": val2}
def parse_insert(line):
    if line.startswith('--'):
        return None

    line_parsed = re.findall('INSERT\s+INTO\s+(.+)\s*\((.+)\)\s*VALUES\s*\((.+)\)\s*;', line, re.IGNORECASE)
    result = OrderedDict()

    if len(line_parsed) > 0:
        result["TABLE"] = line_parsed[0][0].strip()
        keys = line_parsed[0][1].split(",")
        values = [value for value in line_parsed[0][2].split(",")]
        for (key, value) in zip(keys, values):
            result[key.strip().upper()] = value.upper().strip()
    
    return result

