#!/usr/bin/env python3
# coding: utf-8

def convert_to_tuplist(dictlist):
    tuplist = [row.values() for row in dictlist] 
    tuplist.insert(0, dictlist[0].keys())

    return tuplist

def convert_dictlist_to_table(dictlist, print_header):
    tuplist = convert_to_tuplist(dictlist)
    if print_header:
        s = [[str(e) for e in row] for row in tuplist]
    else:
        s = [[str(e) for e in row] for row in tuplist[1:]]
    lens = [max(map(len, col)) for col in zip(*s)]
    if print_header:
        s.insert(1, ['-'*l for l in lens])
    fmt = '\t'.join('{{{0}:{1}}}'.format(i, x) for i, x in enumerate(lens))
    table = [fmt.format(*row).replace("None", "    ") for row in s]

    return table

def convert_dictlist_to_mkd_table(dictlist):
    tuplist = convert_to_tuplist(dictlist)
    s = [[str(e) for e in row] for row in tuplist]
    lens = [max(map(len, col)) for col in zip(*s)]
    s.insert(1, ['-'*l for l in lens])
    fmt = '|'.join('{{{0}:{1}}}'.format(i, x) for i, x in enumerate(lens))
    table = ['|' + fmt.format(*row) + '|' for row in s]

    return table

def convert_dictlist_to_jira_table(dictlist):
    tuplist = convert_to_tuplist(dictlist)
    s = [[str(e) for e in row] for row in tuplist]
    lens = [max(map(len, col)) for col in zip(*s)]
    fmt = '|'.join('{{{0}:{1}}}'.format(i, x) for i, x in enumerate(lens))
    table = ['|' + fmt.format(*row) + '|' for row in s]
    table[0] = table[0].replace('|', '||')

    return table

def convert_dictlist_to_csv_table(dictlist, print_header=True, separator=';'):
    table = list()

    if print_header:
        table.append(separator.join(dictlist[0]))

    for row in dictlist:
        table.append(separator.join([str(val) if val is not None else "" for val in row.values()]))

    return table

class textfile_writer:
    fhandle = None

    def __init__(self, filename):
        self.fhandle = open(filename, 'w')
    
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.fhandle.write('\n')
        self.fhandle.close()
        
    def write_text(self, text):
        self.fhandle.write(text)
    
    def write_dictlist_as_table(self, dictlist, print_header=True):
        #print '\n'.join(table)
        table = convert_dictlist_to_table(dictlist, print_header)
        self.fhandle.write('\n'.join(table))

    def write_dictlist_as_mkd_table(self, dictlist):
        #print '\n'.join(table)
        table = convert_dictlist_to_mkd_table(dictlist)
        self.fhandle.write('\n'.join(table))

    def close(self):
        self.fhandle.close()

class stdout_writer:
    def print_dictlist_as_table(self, dictlist, print_header=True):
        if dictlist == None or len(dictlist) == 0:
            print("no rows selected")
        else:
            table = convert_dictlist_to_table(dictlist, print_header)
            print('\n'.join(table))

    def print_dictlist_as_mkd_table(self, dictlist):
        if dictlist == None or len(dictlist) == 0:
            print("no rows selected")
        else:
            table = convert_dictlist_to_mkd_table(dictlist)
            print('\n'.join(table))

    def print_dictlist_as_jira_table(self, dictlist):
        if dictlist == None or len(dictlist) == 0:
            print("no rows selected")
        else:
            table = convert_dictlist_to_jira_table(dictlist)
            print('\n'.join(table))

    def print_dictlist_as_csv_table(self, dictlist, print_header=True, separator=';'):
        if dictlist == None or len(dictlist) == 0:
            print("no rows selected")
        else:
            table = convert_dictlist_to_csv_table(dictlist, print_header, separator)
            print('\n'.join(table))
